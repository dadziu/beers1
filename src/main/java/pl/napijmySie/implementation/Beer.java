package pl.napijmySie.implementation;

import org.springframework.stereotype.Component;

import pl.napijmySie.api.IBeer;

@Component//optional
//@PropertySource("classpath:properties/beers.properties")
public class Beer implements IBeer {

    private String name;
    private double alcoholPercentage;
    private double price;

/*    public Beer(
            @Value("${name_warka}") String name,
            @Value("${alcohol_warka}") double alcoholPercentage),
            @Value ("${price_warka}") double price {
        super();*/

    public Beer(String name, double alcoholPercentage, double price) {
        super();

        this.name = name;
        this.alcoholPercentage = alcoholPercentage;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getAlcoholPercentage() {
        return alcoholPercentage;
    }

    public double getPrice() {
        return price;
    }
}
