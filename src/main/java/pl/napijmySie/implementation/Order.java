package pl.napijmySie.implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.napijmySie.api.IBeer;
import pl.napijmySie.api.IOrder;

import java.util.List;

@Component//optional
public class Order implements IOrder {

    @Value("#{beers[4]}") //(2) takes value from List of 'beers()' method (from Bean)
    private IBeer beer;


    public Order (List<IBeer> beer) {
        super();
    }//(1) this class will take list of beers as parameter from Beans


    public void showOrder() {
        System.out.printf("nazwa: " + beer.getName() + "%n" +
                "procenty: " + beer.getAlcoholPercentage() + "%%" + "%n" +
                "cena: " + beer.getPrice() + "PLN" + "%n");
    }

}
