package pl.napijmySie.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pl.napijmySie.api.IBeer;
import pl.napijmySie.api.IOrder;
import pl.napijmySie.implementation.Order;
import pl.napijmySie.implementation.Beer;

import java.util.ArrayList;
import java.util.List;


@Configuration
public class Config {

    @Bean
    public List<IBeer> beers(){ //(2)
        List<IBeer> beers = new ArrayList<>();
        beers.add(new Beer("Warka", 4.5 , 3.20));
        beers.add(new Beer("Tyskie", 5.2, 4.10));
        beers.add(new Beer("Lech", 5.5, 3.99));
        beers.add(new Beer("Zywiec", 5.8, 3.70));
        beers.add(new Beer("Ciechan", 6.2, 5.30));
        return beers;
    }

    //(1) injects type: List<IBeer> to Order.class (inputted to constructor)
    @Bean public IOrder order (List<IBeer> beer) { return new Order(beer); }

/*    @Bean
    public IBeer beer() {
        return new Beer("Beer", 4.5, 3.40);
    }
    @Bean
    public IOrder order (IBeer beer) {
        return new Order(beer);
    }*/
}
