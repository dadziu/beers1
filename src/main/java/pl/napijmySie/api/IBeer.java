package pl.napijmySie.api;

public interface IBeer {
    String getName();
    double getAlcoholPercentage();
    double getPrice();
}
