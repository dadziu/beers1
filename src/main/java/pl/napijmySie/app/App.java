package pl.napijmySie.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import pl.napijmySie.api.IOrder;
import pl.napijmySie.config.Config;
import pl.napijmySie.implementation.Order;


public class App {
    public static void main (String[] args){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        IOrder order = context.getBean(Order.class);
        order.showOrder();

    }
}
